---
defaults: []
flags:
- write
minimums: []
name: xlp
types:
- point
used_by: NE
---
Position of an exterior label, [in points](#points).

The position indicates the center of the label.
