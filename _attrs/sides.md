---
defaults:
- '4'
flags: []
minimums:
- '0'
name: sides
types:
- int
used_by: N
---
Number of sides when [`shape`](#d:shape)`=polygon`.
