---
name: point
---
`"%f,%f('!')?"` representing the point `(x,y)`. The optional `'!'` indicates the
node position should not change (input-only).

If [`dim`](#d:dim)`=3`, `point` may also have the format `"%f,%f,%f('!')?"`
to represent the point `(x,y,z)`.
